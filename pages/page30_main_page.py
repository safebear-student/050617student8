from page_objects import PageObject, PageElement

class MainPage(PageObject):
    logout_link = PageElement(link_text="Logout")

    def check_page(self):
        return "Main" in self.w.title

    def click_logout(self, WelcomePage):
        self.logout_link.click()
        return WelcomePage.check_page
